﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OIS_seminarska1
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

        protected void DV_Product_PageIndexChanging(object sender, DetailsViewPageEventArgs e)
        {
            L_Product.Visible = true;
            DV_Product.Visible = true;
        }

        protected void GV_Orders_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GV_Products.SelectedIndex > -1)
            {
                GV_Orders.Visible = true;
                L_Orders.Visible = true;
            }
        }
	}
}