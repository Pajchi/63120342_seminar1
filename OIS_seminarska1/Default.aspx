﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OIS_seminarska1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-size: x-large; color: #008000; font-weight: bold">
    
        Oddelki in skupine izdelkov<br />
        <asp:DropDownList ID="DDL_Categories" runat="server" DataSourceID="ProductCategory_name" DataTextField="Name" DataValueField="ProductCategoryID" AutoPostBack="True" BackColor="#66FF33">
        </asp:DropDownList>
        <br />
        <asp:SqlDataSource ID="ProductCategory_name" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductCategoryID, ParentProductCategoryID, Name, rowguid, ModifiedDate FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID IS NULL)"></asp:SqlDataSource>
    
        <br />
    
    </div>
        <asp:GridView ID="GV_Categories" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" SelectedIndex="0" AllowPaging="True" PageSize="5" DataKeyNames="ProductCategoryID">
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" />
                <asp:CommandField ShowSelectButton="True" ButtonType="Button" SelectText="&gt;" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF33" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT Name, ProductCategoryID FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @ParentProductCategoryID)" UpdateCommand="SELECT Name FROM SalesLT.ProductCategory WHERE (ParentProductCategoryID = @ParentProductCategoryID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="DDL_Categories" Name="ParentProductCategoryID" PropertyName="SelectedValue" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="ParentProductCategoryID" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <br />
        <br />
        <p style="font-size: x-large; color: #008000; font-weight: bold">
            Modeli</p>
        <asp:GridView ID="GV_Models" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource2" DataKeyNames="ProductModelID" SelectedIndex="0">
            <Columns>
                <asp:BoundField DataField="Name" SortExpression="Name" />
                <asp:BoundField DataField="Description" SortExpression="Description" />
                <asp:CommandField ShowSelectButton="True" SelectText="Prikaži izdelke" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF33" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT SalesLT.ProductModel.Name, SalesLT.ProductDescription.Description, SalesLT.ProductModel.ProductModelID FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductModelProductDescription ON SalesLT.ProductModel.ProductModelID = SalesLT.ProductModelProductDescription.ProductModelID INNER JOIN SalesLT.ProductDescription ON SalesLT.ProductModelProductDescription.ProductDescriptionID = SalesLT.ProductDescription.ProductDescriptionID WHERE (SalesLT.ProductModelProductDescription.Culture = N'en') AND (SalesLT.Product.ProductCategoryID = @ProductCategoryID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" DefaultValue="0" Name="ProductCategoryID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <p>
            &nbsp;</p>
        <p style="font-size: x-large; color: #008000; font-weight: bold">
            Izdelki</p>
        <asp:GridView ID="GV_Products" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource3" SelectedIndex="0" DataKeyNames="ProductID">
            <Columns>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra izdelka" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv izdelka" SortExpression="Name" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="ListPrice" HeaderText="Cena" SortExpression="ListPrice" DataFormatString="{0:c}" />
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
                <asp:BoundField DataField="ProductModelID" HeaderText="ProductModelID" InsertVisible="False" ReadOnly="True" SortExpression="ProductModelID" Visible="False" />
                <asp:BoundField DataField="ProductCategoryID" HeaderText="ProductCategoryID" InsertVisible="False" ReadOnly="True" SortExpression="ProductCategoryID" Visible="False" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" Visible="False" />
            </Columns>
            <SelectedRowStyle BackColor="#66FF33" />
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.Product.Name, SalesLT.Product.Size, SalesLT.Product.Color, SalesLT.Product.ListPrice, SalesLT.Product.ProductNumber, SalesLT.ProductModel.ProductModelID, SalesLT.ProductCategory.ProductCategoryID, SalesLT.Product.ProductID FROM SalesLT.Product INNER JOIN SalesLT.ProductModel ON SalesLT.Product.ProductModelID = SalesLT.ProductModel.ProductModelID INNER JOIN SalesLT.ProductCategory ON SalesLT.Product.ProductCategoryID = SalesLT.ProductCategory.ProductCategoryID WHERE (SalesLT.ProductCategory.ProductCategoryID = @ProductCategoryID) AND (SalesLT.ProductModel.ProductModelID = @ProductModelID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Categories" Name="ProductCategoryID" PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="GV_Models" Name="ProductModelID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:Label ID="L_Product" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Green" Text="Podatki o izdelku" AssociatedControlID="DV_Product"></asp:Label>
        <asp:DetailsView ID="DV_Product" runat="server" AutoGenerateRows="False" DataKeyNames="ProductID" DataSourceID="SqlDataSource4" Height="50px" Width="225px" OnPageIndexChanging="DV_Product_PageIndexChanging">
            <Fields>
                <asp:BoundField DataField="ProductNumber" HeaderText="Šifra" SortExpression="ProductNumber" />
                <asp:BoundField DataField="Name" HeaderText="Naziv" SortExpression="Name" />
                <asp:BoundField DataField="Color" HeaderText="Barva" SortExpression="Color" />
                <asp:BoundField DataField="Size" HeaderText="Velikost" SortExpression="Size" />
                <asp:BoundField DataField="Weight" HeaderText="Teža" SortExpression="Weight" />
                <asp:BoundField DataField="StandardCost" DataFormatString="{0:c}" HeaderText="Nabavna cena" SortExpression="StandardCost" />
                <asp:BoundField DataField="ListPrice" DataFormatString="{0:c}" HeaderText="Prodajna cena" SortExpression="ListPrice" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" Visible="False" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT ProductNumber, Name, Color, Size, Weight, StandardCost, ListPrice, ProductID FROM SalesLT.Product WHERE (ProductID = @ProductID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="ProductID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <asp:Label ID="L_Orders" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Green" Text="Seznam naročil" AssociatedControlID="GV_Orders"></asp:Label>
        <asp:GridView ID="GV_Orders" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource5" OnSelectedIndexChanged="GV_Orders_SelectedIndexChanged">
            <Columns>
                <asp:BoundField DataField="SalesOrderID" SortExpression="SalesOrderID" />
                <asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="Datum" SortExpression="OrderDate" />
                <asp:BoundField DataField="Kupec" HeaderText="Kupec" ReadOnly="True" SortExpression="Kupec" />
                <asp:BoundField DataField="OrderQty" HeaderText="Količina" SortExpression="OrderQty" />
                <asp:BoundField DataField="UnitPrice" DataFormatString="{0:c}" HeaderText="Cena enote" SortExpression="UnitPrice" />
                <asp:BoundField DataField="UnitPriceDiscount" DataFormatString="{0:p}" HeaderText="Popust" SortExpression="UnitPriceDiscount" />
                <asp:BoundField DataField="LineTotal" DataFormatString="{0:c}" HeaderText="Cena" ReadOnly="True" SortExpression="LineTotal" />
                <asp:BoundField DataField="ProductID" HeaderText="ProductID" InsertVisible="False" ReadOnly="True" SortExpression="ProductID" Visible="False" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource5" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT SalesLT.SalesOrderDetail.SalesOrderID, SalesLT.SalesOrderHeader.OrderDate, CONCAT(SalesLT.Customer.CompanyName + ' (', SalesLT.Address.City + ', ', SalesLT.Address.CountryRegion + ')') AS 'Kupec', SalesLT.SalesOrderDetail.OrderQty, SalesLT.SalesOrderDetail.UnitPrice, SalesLT.SalesOrderDetail.UnitPriceDiscount, SalesLT.SalesOrderDetail.LineTotal, SalesLT.Product.ProductID FROM SalesLT.SalesOrderDetail INNER JOIN SalesLT.SalesOrderHeader ON SalesLT.SalesOrderDetail.SalesOrderID = SalesLT.SalesOrderHeader.SalesOrderID INNER JOIN SalesLT.Customer ON SalesLT.SalesOrderHeader.CustomerID = SalesLT.Customer.CustomerID INNER JOIN SalesLT.Address ON SalesLT.SalesOrderHeader.ShipToAddressID = SalesLT.Address.AddressID AND SalesLT.SalesOrderHeader.BillToAddressID = SalesLT.Address.AddressID INNER JOIN SalesLT.Product ON SalesLT.SalesOrderDetail.ProductID = SalesLT.Product.ProductID WHERE (SalesLT.Product.ProductID = @ProductID)">
            <SelectParameters>
                <asp:ControlParameter ControlID="GV_Products" Name="ProductID" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
